using System;

namespace Engine
{
    public class Notebook
    {
        private Person[] listPerson;

        public Notebook() 
        {
            this.listPerson = new Person[0];
        }
        public Notebook(Person[] listPers) 
        {
            this.listPerson = listPers;
        }

        public void addPerson(Person pers, int place=-999)
        {
            if (place == -999)
            {
                place = this.listPerson.Length;
            }
            if (place > this.listPerson.Length || place < 0)
            {
                return;
            }

            if (this.listPerson.Length == 0)
            {
                if (place == 0)
                {
                    this.listPerson = new Person[] {pers}; 
                }
                return;
            }

            Person[] tempArray = new Person[this.listPerson.Length + 1];

            for (int index = 0; index < place; index++)
            {
                tempArray[index] = this.listPerson[index];
            }
            tempArray[place] = pers;
            for (int index = place + 1; index < tempArray.Length; index++)
            {
                tempArray[index] = this.listPerson[index - 1];
            }

            this.listPerson = tempArray;
            tempArray = null;
        }

        public void removePerson(int place=-999)
        {
            if (place == -999)
            {
                place = this.listPerson.Length - 1;
            }
            if (place >= this.listPerson.Length || place < 0 || this.listPerson.Length == 0)
            {
                return;
            }

            if (this.listPerson.Length == 1)
            {
                this.listPerson = new Person[0];
                return;
            }

            Person[] tempArray = new Person[this.listPerson.Length - 1];

            for (int index = 0; index < place; index++)
            {
                tempArray[index] = this.listPerson[index];
            }
            for (int index = place; index < tempArray.Length; index++)
            {
                tempArray[index] = this.listPerson[index + 1];
            }

            this.listPerson = tempArray;
            tempArray = null;
        }


        public Person getPerson(int index)
        {
            return this.listPerson[index];
        }

        public void setPerson(Person pers, int index)
        {
            this.listPerson[index] = pers;
        }

        public Person[] getListPerson()
        {
            return this.listPerson;
        }

        public void setListPerson(Person[] persons)
        {
            this.listPerson = persons;
        }

        public string showAllInformationAbout(int index_pers)
        {
            string resultStr = "";

            resultStr += "Имя: " + this.listPerson[index_pers].getStringName() + "\n";
            resultStr += "Номер: " + this.listPerson[index_pers].getNumberPhone() + "\n";
            resultStr += "Город: " + this.listPerson[index_pers].getCountry() + "\n";
            resultStr += "День рождения: " + this.listPerson[index_pers].getStringBirthday() + "\n";
            resultStr += "Организация: " + this.listPerson[index_pers].getOrganization() + "\n";
            resultStr += "Записки: " + this.listPerson[index_pers].showNotes();

            return resultStr;
        }

        public string showAllInformationAboutAll()
        {
            string resultStr = "";

            for (int index = 0; index < this.listPerson.Length - 1; index++)
            {
                resultStr += this.showAllInformationAbout(index) + "\n";
            }
            return resultStr;
        }

        public override string ToString()
        {
            string resultStr = "";

            for (int index = 0; index < this.listPerson.Length - 1; index++)
            {
                resultStr += this.listPerson[index].ToString() + "\n";
            }

            return resultStr;
        }

        public void parseString(string text)
        {
            string[] tempArray = text.Split("\n");
            Person tempPerson;

            for (int index = 0; index < tempArray.Length; index++)
            {
                tempPerson = new Person();
                tempPerson.parseString(tempArray[index]);
                this.addPerson(tempPerson);
                tempPerson = null;
            }
        } 
    }

    public class Person
    {
        private NamePerson name;
        private string numberPhone = "";
        private string country = "";
        private BirthdayPerson birthday;
        private string organization = "";
        private string position = "";
        private string[] notes;
        private bool status = false;

        private void checkOnReady()
        {
            if ((this.name != null) || (this.numberPhone != null && this.numberPhone != "") || (this.country != null && this.country != ""))
            {
                this.status = true;
            }
            else
            {
                this.status = false;
            }
        }


        public override string ToString()
        {
            if (!this.status)
            {
                return "";
            }
            string result = "";

            result += "name:" + this.getStringName() + ";";
            result += "numberPhone:" + this.getNumberPhone() + ";";
            result += "country:" + this.getCountry() + ";";
            result += "birthday:" + this.getStringBirthday() + ";";
            result += "organization:" + this.getOrganization() + ";";
            result += "position:" + this.getPosition() + ";";
            result += "notes:" + this.getStringNotes() + ";";

            return result;
        }

        public void parseString(string text)
        {
            string[] listParam = text.Split(";");

            //костыль
            string[] tempArr = new string[listParam.Length - 1];
            for (int index = 0; index < tempArr.Length; index++)
            {
                tempArr[index] = listParam[index];
            }
            listParam = tempArr;
            tempArr = null;

            for (int index_1 = 0; index_1 < listParam.Length; index_1++)
            {
                string[] tempString = listParam[index_1].Split(':');
                string nameParam = tempString[0];

                // костыль
                string[] tempStr = new string[(tempString.Length - 1)];
                for (int index_2 = 0; index_2 < tempString.Length - 1; index_2++)
                {
                    tempStr[index_2] = tempString[index_2 + 1];
                }
                string argParam = String.Join(":", tempStr);



                tempString = null;
                tempStr = null;

                if (nameParam == "name")
                {
                    this.setStringName(argParam);
                }
                else if (nameParam == "numberPhone")
                {
                    this.setNumberPhone(argParam);
                }
                else if (nameParam == "country")
                {
                    this.setCountry(argParam);
                }
                else if (nameParam == "birthday")
                {
                    this.setStringBirthday(argParam);
                }
                else if (nameParam == "organization")
                {
                    this.setOrganization(argParam);
                }
                else if (nameParam == "position")
                {
                    this.setPosition(argParam);
                }
                else if (nameParam == "notes")
                {
                    this.setStringNotes(argParam);
                }
            }

            this.checkOnReady();
        }

        public NamePerson getName()
        {
            return this.name;
        }

        public void setName(NamePerson name)
        {
            this.name = name;
            this.checkOnReady();
        }

        public string getStringName()
        {
            if (this.name != null)
            {
                return this.name.ToString();
            }
            else
            {
                return "";
            }
        }

        public void setStringName(string name)
        {
            if (this.name == null)
            {
                this.name = new NamePerson("","");
            }
            this.name.parseString(name);
            this.checkOnReady();
        }

        public string getNumberPhone()
        {
            if (this.numberPhone != null)
            {
                return this.numberPhone;
            }
            else
            {
                return ""; 
            }
        }

        public void setNumberPhone(string numberPhone)
        {
            const string symbolsForPhone = "+0123456789";
            bool flag = false;

            for (int index_1 = 0; index_1 < numberPhone.Length; index_1++)
            {
                for (int index_2 = 0; index_2 < symbolsForPhone.Length; index_2++)
                {
                    if (numberPhone[index_1] == symbolsForPhone[index_2])
                    {
                        flag = true;
                        break;
                    }
                }

                if (!flag)
                {
                    return;
                }

                flag = false;
            }
            
            this.numberPhone = numberPhone;
            this.checkOnReady();
        }

        public string getCountry()
        {
            if (this.country != null)
            {
                return this.country;
            }
            else
            {
                return "";
            }
        }

        public void setCountry(string country)
        {
            this.country = country;
            this.checkOnReady();
        }

        public BirthdayPerson getBirthday()
        {
            return this.birthday;
        }

        public void setBirthday(BirthdayPerson birthday)
        {
            this.birthday = birthday;
        }

        public string getStringBirthday()
        {
            if (this.birthday != null)
            {
                return this.birthday.ToString();
            }
            else
            {
                return "";
            }
        }

        public void setStringBirthday(string birthday)
        {
            if (this.birthday == null)
            {
                this.birthday = new BirthdayPerson(0,0,0);
            }
            this.birthday.parseString(birthday);
        }

        public string getOrganization()
        {
            if (this.organization != null)
            {
                return this.organization;
            }
            else
            {
                return "";
            }
        }

        public void setOrganization(string organization)
        {
            this.organization = organization;
        }

        public string getPosition()
        {
            if (this.position != null)
            {
                return this.position;
            }
            else
            {
                return "";
            }
        }

        public void setPosition(string position)
        {
            this.position = position;
        }

        public string[] getNotes()
        {
            return this.notes;
        }

        public void setNotes(string[] notes)
        {
            this.notes = notes;
        }

        public string getStringNotes()
        {
            if (this.notes == null || this.notes.Length == 0)
            {
                return "";
            }

            if (this.notes.Length == 1)
            {
                return notes[0]; 
            }

            string result = "[";

            for (int index = 0; index < this.notes.Length - 1; index++)
            {
                result += this.notes[index] + ",";
            }
            result += this.notes[this.notes.Length - 1] + "]";

            return result;
        }

        public void setStringNotes(string notes)
        {
            notes = notes.Replace("[", "");
            notes = notes.Replace("]", "");

            this.notes = notes.Split(",");
            
            if (this.notes.Length == 1 && this.notes[0] == "")
            {
                this.notes = new string[0];
            }
        }

        public void addNotes(string text, int place=-999)
        {
            if (place == -999)
            {
                place = this.notes.Length;
            }
            if (place > this.notes.Length || place < 0)
            {
                return;
            }

            if (this.notes.Length == 0)
            {
                if (place == 0)
                {
                    this.notes = new string[] {text}; 
                }
                return;
            }

            string[] tempArray = new string[this.notes.Length + 1];

            for (int index = 0; index < place; index++)
            {
                tempArray[index] = this.notes[index];
            }
            tempArray[place] = text;
            for (int index = place + 1; index < tempArray.Length; index++)
            {
                tempArray[index] = this.notes[index - 1];
            }

            this.notes = tempArray;
            tempArray = null;
        }

        public void removeNotes(int place=-999)
        {
            if (place == -999)
            {
                place = this.notes.Length - 1;
            }
            if (place >= this.notes.Length || place < 0 || this.notes.Length == 0)
            {
                return;
            }
            if (this.notes.Length == 1)
            {
                this.notes = new string[0];
                return;
            }

            string[] tempArray = new string[this.notes.Length - 1];

            for (int index = 0; index < place; index++)
            {
                tempArray[index] = this.notes[index];
            }
            for (int index = place; index < tempArray.Length; index++)
            {
                tempArray[index] = this.notes[index + 1];
            }

            this.notes = tempArray;
            tempArray = null;
        }

        public string showIndexNotes(int index)
        {
            return this.notes[index];
        }

        public string showNotes()
        {
            if (this.notes.Length == 0)
            {
                return "";
            }
            string resultStr = "";

            for (int index = 0; index < this.notes.Length - 1; index++)
            {
                resultStr += this.notes[index] + "\n";
            }

            resultStr += this.notes[this.notes.Length - 1];

            return resultStr;
        }
    }

    public class NamePerson
    {
        string firstName = "";
        string secondName = "";
        string thirdName = "";

        public NamePerson(string firstName, string secondName, string thirdName = "")
        {
            this.firstName = firstName;
            this.secondName = secondName;
            this.thirdName = thirdName;
        }

        public override string ToString()
        {
            if (this.thirdName != null)
            {
                return Convert.ToString(this.firstName) + " " + Convert.ToString(this.secondName) + " " + Convert.ToString(this.thirdName);
            }
            else
            {
                return Convert.ToString(this.firstName) + " " + Convert.ToString(this.secondName);
            }
        }

        public void parseString(string fullName)
        {
            string[] tempArr = fullName.Split(" ");

            if (tempArr.Length != 3)
            {
                this.firstName = "";
                this.secondName = "";
                this.thirdName = "";
            }
            else
            {
                this.firstName = tempArr[0];
                this.secondName = tempArr[1];
                this.thirdName = tempArr[2];
            }
        }

        public string getFirstName()
        {
            return this.firstName;
        }
        public void setFirstName(string name)
        {
            this.firstName = name;
        }
        public string getSecondName()
        {
            return this.secondName;
        }
        public void setSecondName(string name)
        {
            this.secondName = name;
        }
        public string getThirdName()
        {
            return this.thirdName;
        }
        public void setThirdName(string name)
        {
            this.thirdName = name;
        }
    }

    public class BirthdayPerson
    {
        int day;
        int month;
        int year;

        public BirthdayPerson(int day, int month, int year)
        {
            this.setDay(day);
            this.setMonth(month);
            this.setYear(year);
        }

        public string ToString(string separetor = ".")
        {
            return Convert.ToString(this.day) + separetor + Convert.ToString(this.month) + separetor + Convert.ToString(this.year);
        }

        public void parseString(string text)
        {
            if (text == "  ")
            {
                this.setDay(0);
                this.setMonth(0);
                this.setYear(0);
            }
            const string symbolsForDate = "0123456789";
            int countSeparetor = 0;
            bool flag = false; 

            //Проверка №1
            for (int index_1 = 0; index_1 < text.Length; index_1++)
            {
                for (int index_2 = 0; index_2 < symbolsForDate.Length; index_2++)
                {
                    if (text[index_1] == symbolsForDate[index_2])
                    {
                        flag = true;
                        break;
                    }
                }

                if (!flag)
                {
                    countSeparetor++;
                }

                flag = false;
            }
            if (countSeparetor != 2)
            {
                return;
            }

            //Проверка №2
            for (int index_1 = 0; index_1 < symbolsForDate.Length; index_1++)
            {
                if (text[0] == symbolsForDate[index_1])
                {
                    flag = true;
                    break;
                }
            }
            if (!flag)
            {
                return;
            }
            flag = false;
            countSeparetor = 0;

            string tempStr = "";

            // Обработка
            for (int index_1 = 0; index_1 < text.Length; index_1++)
            {
                for (int index_2 = 0; index_2 < symbolsForDate.Length; index_2++)
                {
                    if (text[index_1] == symbolsForDate[index_2])
                    {
                        flag = true;
                        break;
                    }
                }

                if (flag)
                {
                    tempStr += text[index_1];
                }
                else
                {
                    if (tempStr == "")
                    {
                        tempStr = "0";
                    }

                    if (countSeparetor == 0)
                    {
                        this.setDay(Convert.ToInt32(tempStr));
                        tempStr = "";
                    }
                    else if (countSeparetor == 1)
                    {
                        this.setMonth(Convert.ToInt32(tempStr));
                        tempStr = "";
                    }
                    countSeparetor++;
                }

                flag = false;
            }

            this.setYear(Convert.ToInt32(tempStr));
            tempStr = "";
        }

        public int getDay()
        {
            return this.day;
        }

        public void setDay(int day)
        {
            if (day <= 31 && day >= 1)
            {
                this.day = day;
            }
            else
            {
                this.day = 1;
            }
        }

        public int getMonth()
        {
            return this.month;
        }

        public void setMonth(int month)
        {
            if (month <= 12 && month >= 1)
            {
                this.month = month;
            }
            else
            {
                this.month = 1;
            }
            
        }

        public int getYear()
        {
            return this.year;
        }

        public void setYear(int year)
        {
            this.year = year;
        }
    }
}