using System;

namespace InOut
{
    class Terminal
    {
        private string writeString;
        int countChar = 0;

        public Terminal()
        {
            writeString = "";
        }

        public void addString(string text)
        {
            this.writeString += text;
            this.countChar += text.Length;
        }

        public void pushString()
        {
            Console.Write(this.writeString);
        }

        public string readString()
        {
            string text = Console.ReadLine();
            this.countChar += text.Length;
            return text;
        }

        public void clear(int count = -1)
        {
            if (count == -1)
            {
                count = this.countChar;
                this.writeString = "";
                this.countChar = 0;
            }

            string tempStr = "";

            tempStr += this.multiplyString("\b", count);
            tempStr += this.multiplyString(" ", count);
            tempStr += this.multiplyString("\b", count);

            Console.Write(tempStr);
        }

        public string editString(string text)
        {
            const int step = 5;
            // preparation
            int cursor = 0;
            string tempString = text;
            Console.Write(this.insertChar(text, "|",  cursor));

            // loop
            ConsoleKeyInfo key;
            string tempChar;
            while (true)
            {
                key = Console.ReadKey();
                /// enter
                if (key.Key == ConsoleKey.Enter)
                {
                    return tempString;
                }
                /// esc
                else if (key.Key == ConsoleKey.Escape)
                {
                    Console.Write("\b\n");
                    return text;
                }
                /// arrows
                else if (key.Key == ConsoleKey.LeftArrow)
                {
                    this.clear(1);
                    if (cursor > 0)
                    {
                        cursor--;
                        this.clear(tempString.Length + 1);
                        Console.Write(this.insertChar(tempString, "|",  cursor));
                    }
                }
                else if (key.Key == ConsoleKey.RightArrow)
                {
                    this.clear(1);
                    if (cursor < tempString.Length)
                    {
                        cursor++;
                        this.clear(tempString.Length + 1);
                        Console.Write(this.insertChar(tempString, "|",  cursor));
                    }
                }
                else if (key.Key == ConsoleKey.UpArrow)
                {
                    this.clear(1);
                    if (cursor > (step - 1))
                    {
                        cursor -= step;
                        this.clear(tempString.Length + 1);
                        Console.Write(this.insertChar(tempString, "|",  cursor));
                    }
                }
                else if (key.Key == ConsoleKey.DownArrow)
                {
                    this.clear(1);
                    if (cursor < tempString.Length - (step - 1))
                    {
                        cursor += step;
                        this.clear(tempString.Length + 1);
                        Console.Write(this.insertChar(tempString, "|",  cursor));
                    }
                }
                /// backspace
                else if (key.Key == ConsoleKey.Backspace)
                {
                    this.clear(1);
                    if (cursor > 0)
                    {
                        this.clear(tempString.Length + 1);
                        tempString = this.delChar(tempString, cursor - 1);
                        cursor--;
                        Console.Write(this.insertChar(tempString, "|",  cursor));
                    }
                }
                /// delete
                else if (key.Key == ConsoleKey.Delete)
                {
                    this.clear(1);
                    if (cursor < tempString.Length)
                    {
                        this.clear(tempString.Length + 1);
                        tempString = this.delChar(tempString, cursor);
                        Console.Write(this.insertChar(tempString, "|",  cursor));
                    }
                }
                /// insert char
                else
                {
                    this.clear(1);
                    tempChar = key.KeyChar.ToString();
                    this.clear(tempString.Length + 1);
                    tempString = this.insertChar(tempString, tempChar, cursor);
                    cursor++;
                    Console.Write(this.insertChar(tempString, "|",  cursor));
                }
            }
        }

        private string insertChar(string text, string symbol, int place)
        {
            if (place == 0)
            {
                return symbol + text;
            }
            else if (place == text.Length)
            {
                return text + symbol;
            }

            string result = "";

            for (int index = 0; index < place; index++)
            {
                result += text[index];
            }
            result += symbol;
            for (int index = place; index < text.Length; index++)
            {
                result += text[index];
            }

            return result;
        }

        private string delChar(string text, int place)
        {
            string result = "";

            for (int index = 0; index < place; index++)
            {
                result += text[index];
            }
            for (int index = place + 1; index < text.Length; index++)
            {
                result += text[index];
            }

            return result;
        }

        private string multiplyString(string text, int multiplier)
        {
            return string.Join(text, new string[multiplier + 1]);
        }
    }
}